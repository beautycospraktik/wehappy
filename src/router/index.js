import Vue from 'vue'
import Router from 'vue-router'
import Homepage from '@/components/Homepage'
import AboutUs from '@/components/AboutUs'
import Contact from '@/components/Contact'
import Product from '@/components/product'
import Prices from '@/components/prices'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Homepage',
      component: Homepage
    },
    {
      path: '/about',
      name: 'AboutUs',
      component: AboutUs
    },
    {
      path: '/contact',
      name: 'Contact',
      component: Contact
    },
    {
      path: '/prices',
      name: 'Prices',
      component: Prices
    },
    {
      path: '/product',
      name: 'Product',
      component: Product
    }
  ]
})
